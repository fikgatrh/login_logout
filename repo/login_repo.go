package repo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/twinj/uuid"
	"io/ioutil"
	"login_logout/models"
	"net/http"
	"os"
)

type LoginStruct struct {
	db *gorm.DB
}

type LoginDrupalRepoInterface interface {
	GetData(query []byte) (*models.Document, error)
	DeleteAuth(authD *models.Auth) error
	CreateAuth(username string, sessid string, sessname string, token string, data models.NewDataProduct) (*models.Auth, error)
}

func CreateLoginRepoImpl(db *gorm.DB) LoginDrupalRepoInterface {
	return &LoginStruct{db}
}

func (l *LoginStruct) GetData(query []byte) (*models.Document, error) {
	url := os.Getenv("URL_ELASTIC")

	req, err := http.NewRequest("GET", url, bytes.NewBuffer(query))
	if err != nil {
		fmt.Printf("[GetDataElastic.Repo] Error execute url, %v \n", err)
		return nil, fmt.Errorf("Data is not exist")
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	var response models.Document
	err = json.Unmarshal(body, &response)

	return &response, nil
}

//Once a user row in the auth table
func (l *LoginStruct) DeleteAuth(authD *models.Auth) error {
	db := l.db.Debug().Delete(&authD)
	if db.Error != nil {
		return db.Error
	}
	return nil
}

//Once the user signup/login, create a row in the auth table, with a new uuid
func (l *LoginStruct) CreateAuth(username string, sessid string, sessname string, token string, data models.NewDataProduct) (*models.Auth, error) {
	au := &models.Auth{}

	au.AuthUUID = uuid.NewV4().String() //generate a new UUID each time
	au.Username = username
	au.SessID = sessid
	au.SessionName = sessname
	au.Token = token
	au.Data = data
	err := l.db.Create(&au).Error
	if err != nil {
		return nil, err
	}
	fmt.Println("Insert data to database success")
	return au, nil
}
