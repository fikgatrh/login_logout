package app

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/subosito/gotenv"
	"login_logout/controllers"
	"login_logout/driver"
	"login_logout/repo"
	"login_logout/services"
	"os"
)

func init() {
	gotenv.Load()
}

func StartApp() {
	port := os.Getenv("PORT_LOGIN")
	router := gin.New()
	db := driver.ConnectDB()
	defer db.Close()

	loginRepo := repo.CreateLoginRepoImpl(db)
	loginService := services.CreateLoginService(loginRepo)

	controllers.CreateLoginController(router, loginService)

	fmt.Println("about to start the application...")

	router.Run(port)
}
