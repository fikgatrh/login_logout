package models

// Document ...
type Document struct {
	HitsFix Hits         `json:"hits"`
	//Agg     aggregations `json:"aggregations"`
}

// Hits ...
type Hits struct {
	Totals    total   `json:"total"`
	MaxScore  int     `json:"max_score"`
	HitsArray []hitss `json:"hits"`
}

// total ...
type total struct {
	Value    int    `json:"value"`
	Relation string `json:"relation"`
}

// hitss ...
type hitss struct {
	Index   string `json:"_index"`
	Sources source `json:"_source"`
}

// source ...
type source struct {
	PartnerDevApps []dataPartner `json:"partner_dev_apps"`
}

type dataPartner struct {
	Sessid      string        `json:"sessid"`
	SessionName string        `json:"session_name"`
	Token       string        `json:"token"`
	User        DataUser      `json:"user"`
	DevAppName  string        `json:"dev_app_name"`
	Products    []DataProduct `json:"products"`
}

type DataProduct struct {
	ProductName string `json:"product_name"`
}

// data user ...
type DataUser struct {
	UID    string `json:"uid"`
	Name   string `json:"name"`
	Mail   string `json:"mail"`
	Theme  string `json:"theme"`
	Status string `json:"status"`
}

type ErrorResponse struct {
	Success bool   `json:"succsess"`
	Message string `json:"message"`
}
// aggregations ...
//type aggregations struct {
//	TimePerHit timerPerHit `json:"TimePerHit"`
//}

// timerPerHit ...
//type timerPerHit struct {
//	Buckets []Bucket `json:"buckets"`
//}

// Bucket ...
//type Bucket struct {
//	KeyAsString string `json:"key_as_string"`
//	DocCount    int    `json:"doc_count"`
//}
