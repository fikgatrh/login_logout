package models

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"github.com/jinzhu/gorm"
)

type Auth struct {
	gorm.Model
	AuthUUID    string         `gorm:"size:255;not null;" json:"auth_uuid"`
	Username    string         `gorm:";not null;" json:"username"`
	SessID      string         `json:"sess_id"`
	SessionName string         `json:"session_name"`
	Token       string         `json:"token"`
	Data        NewDataProduct `sql:"type:jsonb"`
}

type NewDataProduct struct {
	DevAppName string     `json:"dev_app_name"`
	Product    []Products `json:"products"`
}

type Products struct {
	ProductName string `json:"product_name"`
}

type Decrypt struct {
	Encrypt string `json:"encrypt"`
}


func InitTable(db *gorm.DB) {
	db.DropTableIfExists(&Auth{})
	db.AutoMigrate(&Auth{})
}

func (n *NewDataProduct) Scan(src interface{}) error {
	bs, ok := src.([]byte)
	if !ok {
		return errors.New("not a []byte")
	}
	return json.Unmarshal(bs, n)
}

func (n NewDataProduct) Value() (driver.Value, error) {
	return json.Marshal(n)
}
