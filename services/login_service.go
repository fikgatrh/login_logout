package services

import (
	"fmt"
	"login_logout/auth"
	"login_logout/models"
	"login_logout/repo"
)

type LoginServiceInterface interface {
	LoginDrupal() (*models.Auth, error)
	DeleteAuth(authD *models.Auth) error
	CreateAuth(username string, sessid string, sessname string, token string, data models.NewDataProduct) (*models.Auth, error)
	SignIn(authD models.Auth) (string, error)
}

type LoginServiceImpl struct {
	loginRepo repo.LoginDrupalRepoInterface
}

func CreateLoginService(loginRepo repo.LoginDrupalRepoInterface) LoginServiceInterface {
	return &LoginServiceImpl{loginRepo}
}

func (l *LoginServiceImpl) LoginDrupal() (*models.Auth, error) {
	dataRespon, err := l.loginRepo.GetData([]byte(""))
	if err != nil {
		fmt.Printf("[LoginServiceImpl.Service] Error when GetdataElastic to repo, %v \n", err)
		return nil, err
	}

	token := dataRespon.HitsFix.HitsArray[0].Sources.PartnerDevApps[0].Token
	if token == "" {
		fmt.Printf("[LoginServiceImpl.Service] Error when validate data token: %v\n", err)
		return nil, err
	}

	currentSessID := dataRespon.HitsFix.HitsArray[0].Sources.PartnerDevApps[0].Sessid
	currentSessionName := dataRespon.HitsFix.HitsArray[0].Sources.PartnerDevApps[0].SessionName
	currentToken := dataRespon.HitsFix.HitsArray[0].Sources.PartnerDevApps[0].Token
	currentDevAppName := dataRespon.HitsFix.HitsArray[0].Sources.PartnerDevApps[0].DevAppName

	var dataFix models.Auth
	var currentDataProduct models.NewDataProduct
	var arrayDataProduct []models.Products
	length := len(dataRespon.HitsFix.HitsArray[0].Sources.PartnerDevApps[0].Products)

	for i := 0; i < length; i++ {
		var tempData models.Products
		tempData.ProductName = dataRespon.HitsFix.HitsArray[0].Sources.PartnerDevApps[0].Products[i].ProductName
		arrayDataProduct = append(arrayDataProduct, tempData)
	}

	dataFix.SessID = currentSessID
	dataFix.SessionName = currentSessionName
	dataFix.Token = currentToken
	currentDataProduct.DevAppName = currentDevAppName
	currentDataProduct.Product = arrayDataProduct
	dataFix.Data = currentDataProduct

	return &dataFix, nil
}

func (l *LoginServiceImpl) DeleteAuth(authD *models.Auth) error {
	return l.loginRepo.DeleteAuth(authD)
}

func (l *LoginServiceImpl) CreateAuth(username string, sessid string, sessname string, token string, data models.NewDataProduct) (*models.Auth, error) {
	return l.loginRepo.CreateAuth(username, sessid, sessname, token, data)
}

func (l *LoginServiceImpl) SignIn(authD models.Auth) (string, error) {
	token, err := auth.CreateToken(authD)
	if err != nil {
		return "", err
	}
	return token, nil
}
