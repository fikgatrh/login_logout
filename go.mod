module login_logout

go 1.13

require (
	github.com/badoux/checkmail v0.0.0-20181210160741-9661bd69e9ad
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.13
	github.com/subosito/gotenv v1.2.0
	github.com/twinj/uuid v1.0.0
)
