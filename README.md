# README #

This feature login for golang application

### What is this repository for? ###

* Login Feature with encrypt and decrypt
* Logout Feature
* JWT

### How do I get set up? ###

* Dependencies using :
    * github.com/badoux/checkmail
    * github.com/dgrijalva/jwt-go
    * github.com/gin-gonic/gin
    * github.com/jinzhu/gorm
    * github.com/myesui/uuid
    * github.com/subosito/gotenv
    * github.com/twinj/uuid
* Database configuration using postgre
* use go mod

### Contribution guidelines ###

* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
