package utils

import (
	"errors"
	"github.com/badoux/checkmail"
)

// ValidateLogin ...
//func ValidateLogin(c *gin.Context, user *models.User) bool {
//	if user.Email == "" {
//		ErrorMessage(c, http.StatusBadRequest, "Email cannot be empty")
//		fmt.Println("[ValidateLogin] Error when validate email")
//		return false
//	}
//	if user.Password == "" {
//		ErrorMessage(c, http.StatusBadRequest, "Password cannot be empty")
//		fmt.Println("[ValidateLogin]")
//		return false
//	}
//	return true
//}

// Validate Email ...
func ValidateEmail(email string) error {
	if email == "" {
		return errors.New("required email")
	}
	if email != "" {
		if err := checkmail.ValidateFormat(email); err != nil {
			return errors.New("invalid email")
		}
	}
	return nil
}
