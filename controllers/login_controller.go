package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"login_logout/auth"
	"login_logout/middlewares"
	"login_logout/models"
	"login_logout/services"
	"login_logout/utils"
	"net/http"
	"os"
)

type LoginDrupalController struct {
	loginDrupalService services.LoginServiceInterface
}

func CreateLoginController(router *gin.Engine, loginDrupalService services.LoginServiceInterface)  {
	inDB := LoginDrupalController{loginDrupalService}

	router.POST("/login", inDB.Login)
	router.POST("/logout", middlewares.TokenAuthMiddleware(), inDB.Logout)
}

func (l *LoginDrupalController) Login(c *gin.Context) {
	key := os.Getenv("KEY_DECRYPT")
	var encrpytData models.Decrypt

	err := c.ShouldBindJSON(&encrpytData)
	if err != nil {
		utils.ErrorMessage(c, http.StatusBadRequest, "Ops, Error when bind json from body")
		fmt.Printf("[login Controller] error when encode data enkripsi : %v\n", err)
		return
	}

	decrypt, err := utils.KeyDecrypt(key, encrpytData.Encrypt)
	if err != nil {
		utils.ErrorMessage(c, http.StatusBadRequest, "Ops, Error when decrypt data")
		fmt.Printf("[login Controller] error when decrypt data enkripsi : %v\n", err)
		return
	}

	result := models.User{}
	err = json.Unmarshal([]byte(decrypt), &result)
	if err != nil {
		utils.ErrorMessage(c, http.StatusInternalServerError, "Ops Error when unmarshal data decrypt into struct")
		fmt.Printf("[login Controller] error when decrypt data enkripsi to struct : %v\n", err)
		return
	}

	isValid := utils.ValidateEmail(result.Username)
	if isValid != nil {
		utils.ErrorMessage(c, http.StatusBadRequest, "this username not email")
		fmt.Println("GA MASUK KE DRUPAL KARENA INI BUKAN EMAIL")
		return
	}

	au, err := l.loginDrupalService.LoginDrupal()
	if err != nil {
		utils.ErrorMessage(c, http.StatusBadRequest, "Ops, error when login service")
		fmt.Printf("[login Controller] error when login service : %v\n", err)
		return
	}

	//since after the usefmt.Println(au.Username)r logged out, we destroyed that record in the database so that same jwt token can't be used twice. We need to create the token again
	authData, err := l.loginDrupalService.CreateAuth(result.Username, au.SessID, au.SessionName, au.Token, au.Data)
	if err != nil {
		utils.ErrorMessage(c, http.StatusInternalServerError, err.Error())
		return
	}

	var authD models.Auth
	authD.AuthUUID    = authData.AuthUUID
	authD.Username    = authData.Username
	authD.SessID   	  = authData.SessID
	authD.SessionName = authData.SessionName
	authD.Token       = authData.Token
	authD.Data		  = authData.Data

	token, loginErr := l.loginDrupalService.SignIn(authD)
	if loginErr != nil {
		utils.ErrorMessage(c, http.StatusForbidden, "Please try to login later")
		return
	}

	var JWT models.TokenStruct

	JWT.Token = token

	c.JSON(http.StatusOK, JWT)

}

func (l *LoginDrupalController) Logout(c *gin.Context) {
	au, err := auth.ExtractTokenAuth(c.Request)
	if err != nil {
		utils.ErrorMessage(c, http.StatusUnauthorized, "Unauthorized")
		return
	}

	delErr := l.loginDrupalService.DeleteAuth(au)
	if delErr != nil {
		utils.ErrorMessage(c, http.StatusUnauthorized, "Unauthorized")
		return
	}
	utils.SuccessMessage(c, http.StatusOK, "Successfully logged out")
}
